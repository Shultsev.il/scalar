import unittest
from scalar import biggestPath

class TestBiggestPath(unittest.TestCase):

	def test_first(self):
		d1 = {'dir1': {}, 'dir2': ['file1'], 'dir3': {'dir4': ['file2'], 'dir5': {'dir6': {'dir7': {}}}}}
		self.assertEqual(biggestPath(d1), '/dir3/dir5/dir6/dir7')

	def test_second(self):
		d2 = {'dir1': ['file1', 'file1']}
		self.assertEqual(biggestPath(d2), '/')

	def test_third(self):
		d3 = {'dir1': ['file1', 'file2', 'file2']}
		self.assertEqual(biggestPath(d3), '/dir1/file1')

	def test_incorrect_symbol(self):
		d4 = {'dir1': ['fi!le1', 'fi!le2', 'fi!le2']}
		self.assertEqual(biggestPath(d4), '/')
	
	def test_long_path256(self):
		d4 = {'dir1': ['file1'],'dir2':{'dir3':['LongFileNameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee']}}
		self.assertEqual(biggestPath(d4), '/dir1/file1')

	def test_long_path255(self):
		d4 = {'dir1': ['file1'],'dir2':{'dir3':['LongFileNameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee']}}
		self.assertEqual(biggestPath(d4), '/dir2/dir3/LongFileNameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')

if __name__ == '__main__':
	# d3 = {'dir1': {}, 'dir2': ['file1'], 'dir3': {'dir4': ['file2'], 'dir5': {'dir6': {'dir7': {}}}}}
	# biggestPath(d3)
	unittest.main()