import re

def biggestPath(X: dict) -> str:
	maxPath=0
	listPath=[]
	def testCorrectPath(currentPath: list, branch: int ) -> bool:

		nonlocal maxPath, listPath

		lenPath=len(currentPath)
		for name in currentPath:
			if(not re.match("""^[a-zA-Z0-9]+$""", name)):
				return False
			lenPath+=len(name)

		if lenPath > 255:
			return False
		
		if maxPath < branch+1:
			maxPath = branch+1
			listPath = currentPath[:maxPath+1]

		return True


	def searchPath(currentDict: dict, currentPath: list, branch: int):
		for element in currentDict:

			if(type(currentDict[element]) is dict):
					currentPath.append(element)
					if testCorrectPath(currentPath,branch):
						searchPath(currentDict[element], currentPath, branch+1)
					currentPath.pop()

			elif(type(currentDict[element]) is list):
				currentPath.append(element)
				unique=[fileName for fileName in currentDict[element] if currentDict[element].count(fileName)==1]
				
				for file in unique:
					currentPath.append(file)
					testCorrectPath(currentPath,branch+1)
					currentPath.pop()

				currentPath.pop()

		return 

	searchPath(X, list(), 0)

	return "/"+"/".join(listPath)

